from PIL import Image


def getAvgColor(imagePath):

	""" 
	Returns a tuple containing the average RGB color
	of a single image.

	@arg1: An image path. (NOT AN IMAGE)
	"""		

	image = Image.open(imagePath)
	(width, height) = image.size

	numPixels 	= 0
	totalRed 	= 0
	totalBlue 	= 0
	totalGreen 	= 0

	image = image.load()			# can now access pixels

	# Iterates through all of the pixels and stores into variables
	for x in range(0, width):
		for y in range(0, height):

			pixel		= image[x,y]
			numPixels	+= 1
			totalRed 	+= pixel[0]
			totalGreen 	+= pixel[1]
			totalBlue	+= pixel[2]

	output = (totalRed/numPixels, totalGreen/numPixels, totalBlue/numPixels)
	return output




def overlap(output, nearest, box):

	output = output.load()
	nearest = nearest.load()
	for x in range(0, 75):
		for y in range(0, 75):
			output[box[0] + x, box[2] + y] = nearest[x, y]

