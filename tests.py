from kdtree import KDTree
from PIL import Image
import glob, os

def kdtree_test():
	data = [(1,2,3),(4,0,1), (5, 3, 1), (10,5,4), (9,8,9), (4,2,4)]
	tree = KDTree.construct_from_data(data)
	nearest = tree.query((5, 4, 3));
	print nearest

def image_test():
	
	""" Rotates an image 45 degrees """
	if False:
		im = Image.open("profile.jpg")
		im.rotate(45).show()


	""" Creates 128 x 128 thumbnails in the cwd """
	""" Comments: Need to read the documentation.
	these are not true 128 x 128 thumbnails; they
	are still rectangles, not squares """
	if True:
		size = (128, 128)
		for infile in glob.glob("images/*.jpg"):
			(name, ext) = os.path.splitext(infile)
			im = Image.open(infile)
			im.thumbnail(size, Image.ANTIALIAS)
			im.save(name + ".thumbnail", "JPEG")
	
	""" Crops. Input is a 4-tuple box, wxyz """
	""" w: x position of left boundary """
	""" x: y position of upper boundary """
	""" y: x position of right boundary """
	""" z: y position of lower boundary """
	if False:
		box = (0, 40, 512, 256)
		(name, ext) = os.path.splitext("images/profile.jpg")
		im = Image.open("images/profile.jpg")
		cropped = im.crop(box)
		cropped.show()
		cropped.save(name + ".cropped", "JPEG")

	""" Edits a patch of pixels from an image """
	if False:
		image = Image.open("images/profile.jpg")
		pix = image.load()
	
		# fucks up a patch in the image
		for x in range(50, 256):
			for y in range(90, 250):
				pix[x,y] = pix[y,x]
		
		print pix[30, 30]
		image.show()

from libimages import *

def libimages_test():

	if True:
		rbg = getAvgColor("images/profile.jpg")
		print rbg

from sys import stdout
from time import sleep

def stdout_test():
	
	for i in range(1, 20):
		stdout.write("\r%d" % i)
		stdout.flush()
		sleep(1)
	
	stdout.write("\n")
