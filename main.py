#!/usr/bin/python

from libimages 	import *
from sys 		import stdout
from kdtree		import KDTree
import gc, os, glob, sys
def main(argv):

	"""
	argv should be a list of three items:
	1. Path of underlying image
	2. Path of directory with pictures
	3. The output image name
	"""
	# Imports the data needed
	os.system("clear")
	gc.enable()
	(underly_image, picdir, output) = parseInput(argv)
	(tree, dictionary) = createData(picdir)
	
	# From here, creates the output
	outputImage = createOutput(underly_image, tree, dictionary)
	outputImage.save(output)


def parseInput(argv):

	if len(argv) != 4:
		failure()

	# Tests whether the image is valid
	try:
		image = Image.open(argv[1])
		image.verify()
	except IOError:
		print "Error: Image Path is invalid"
		failure()

	# Tests whether the directory is valid
	if argv[len(argv)-1] != "/":
		argv[2] += "/"

	if not os.path.isdir(argv[2]):
		print "Error: Image Directory is invalid"
		failure()

	# Checks whether the output image has the same extension
	# as the pictures in the image directory.
	# If it isn't, we change the extension.
	(name, ext) = os.path.splitext(argv[3])
	if ext != ".jpg":
		ext = ".jpg"
		print "Changing output file extension to jpg"

	return (argv[1], argv[2], name + ext)	


def failure():
	print "Usage: " + sys.argv[0] + " <Image Path> <Image Dir> <Image Ext> <Output Name>"
	sys.exit(2)

def createData(picdir):
	""" 
	Returns a 2-tuple containing a KD-tree and a dictionary
	"""
	dictionary 	= {}
	points		= []
	picNum		= 0
	print "Populating the data structure..."

	for infile in glob.glob(picdir + "*.png"):
		stdout.write("\r%d pictures added" % picNum)

		(name, ext) = os.path.splitext(infile)

		rgb = getAvgColor(infile)					# create the key, rgb value
		dictionary[rgb] = infile					# add to the dictionary
		points.append(rgb)

		picNum += 1
		stdout.flush()

	tree = KDTree.construct_from_data(points)		# creates the KD tree
	stdout.write("\n")

	print "Populated data structure with %d pictures" % picNum

	return (tree, dictionary)



def createOutput(underly_image, tree, dictionary):

	""" Creates the final output sought by the user """
	print "Creating output image..."

	# Prepping for operations
	image = Image.open(underly_image)
	(width, height) = image.size
	print "Total size is " + str(width * height)

	output = Image.new("RGBA", (width * 75, height * 75))
	
	# Populates the output
	image = image.load()
	tileNum = 0
	for x in range(0, width):
		for y in range(0, height):
			
			stdout.write("\r%d tiles placed" % tileNum)
			
			pixel 		= image[x,y]
			nearestAvg 	= tree.query(pixel)
			nearestImg 	= dictionary[nearestAvg[0]]
			box 		= (x*75, y*75, x*75 + 75, y*75 + 75)

			nearImage	= Image.open(nearestImg)

			# Pastes the tile onto the output image
		#	try: 
			output.paste(nearImage, box)
		#	except ValueError:
		#		continue	
			"""	
			hello = output.load()
			goodbye = nearImage.load()
			for x in range(0, 75):
				for y in range(0, 75):
					hello[box[0] + x, box[2] + y] = goodbye[x, y]
			"""
			tileNum += 1
			stdout.flush()

	stdout.write("\n")
	print "Placed %d tiles on output image" % tileNum
	return output


if __name__ == "__main__":
	main(sys.argv[0:])
